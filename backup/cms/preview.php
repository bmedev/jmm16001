<html>
<head>
<title>Preview</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="../css/cadogan_styles.css" type="text/css">
</head>

<body>
<span class="biogText">
<script type="text/javascript">
	
	function nl2br(text)
	{	
		
		text = escape(text);
		re_nlchar = "";	
		if(text.indexOf('%0D%0A') > -1){		re_nlchar = /%0D%0A/g ;	}
		else if(text.indexOf('%0A') > -1){		re_nlchar = /%0A/g ;	}
		else if(text.indexOf('%0D') > -1){		re_nlchar = /%0D/g ;	}
		if(re_nlchar!="")
		{
			
			return unescape( text.replace(re_nlchar,'<br />') );
		}
		else
		{
			
			return unescape(text);
		}
	}
	var parText = window.opener.document.getElementById("contents").value;
	parText = nl2br(parText);
	document.write(parText);
	
</script>
</span>
</body>
</html>
