<?
function show_file_structure($content_path, $extensions, $print, $level=0)
{
	
	$output = "";
	$count = 1;
	if(substr($content_path, strlen(trim($content_path))-1, 1)!="/") { $content_path .= '/'; }
	if(file_exists($content_path))
	{
		$dir=opendir($content_path);
		unset($sorting_array); 	
		while($file=readdir($dir))
		{
		   if ($file!="." and $file!="..") 
		   {
				$sorting_array[] = $file;
		   }
		}
		
		if(count($sorting_array)>0)
		{
			sort($sorting_array);
			foreach($sorting_array as $file)
			{
				  if (is_dir($content_path.$file)) 
				  {
					//echo $count." (dir: ".$file.")<br />";
					
					$tmpCount = $level*8;
					
					$output .= "<div class='menuCat' style='margin-left: ".$tmpCount."px'>".$file.'</div>';
					
					$output .= show_file_structure($content_path.$file, $extensions, "n", $level+1);
				  }
				  else
				  {
				  //	echo $count." (file)<br />";
						if(strlen(trim($extensions))>0)
						{	
							$myExp = explode(".",$file);
							$myExt = trim(strtolower($myExp[count($myExp)-1]));
							unset($valid_exts);
							//echo $extensions."<br />";
							$validExp = explode("|", trim(strtolower($extensions)));
							foreach($validExp as $val)
							{
								$valid_exts[$val] = "y";
							}
						}
						if((strlen(trim($extensions))==0)||(isset($myExt) && isset($valid_exts[$myExt]) && $valid_exts[$myExt] == "y"))
						{
							if(strlen(trim($file))>29)
							{
								$trimmedName = substr($file,0,27)."..";
							}
							else
							{
								$trimmedName = $file;
							}
							$trimmedName = str_replace("_"," ",$trimmedName);
							$trimmedName = str_replace(".txt","",$trimmedName);
							for($i=0; $i<$level; $i++)
							{
								$output .= '&nbsp;&nbsp;';
							}
							$output .= '<img src="images/green_dot.gif" width="9" height="7" /> <a href="'.$_SERVER['PHP_SELF'].'?file='.urlencode($content_path.$file).'&p=edit" class="menuFile" title="'.$file.'">'.$trimmedName."</a><br />";
						}
				  }
				  $count++;
			 } 
		}
		
		$output .= "<br />";
		closedir($dir); 
	}
	else
	{
		$output .= "Content Path does not exist.<br />";
	}
	
	if($print=="y") { echo $output; }
	else {	return $output; }
}
?>