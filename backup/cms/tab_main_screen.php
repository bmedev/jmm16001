<?php
if(isset($p) && strlen(trim($p))>0)
{
	if($p=="edit")
	{
		echo '<div class="mainHolderHeader">Edit File</div>';
		echo '<div class="mainHolder">';
		if(strlen(trim($file))>0)
		{
			$fileExp = explode("/",$file);
			$friendly_name = $fileExp[count($fileExp)-1];
			if(file_exists($file))
			{
				if($fp = fopen($file, "r"))
				{
					if($text = fread($fp, filesize($file)))
					{
						if(strlen(trim($error))>0) { echo '<p class="error">'.$error.'</p>'; }
						if(!isset($a) || $a!="update") { $contents = str_replace("<br>","\n",str_replace("<br />","\n",str_replace("<br >","\n",$text))); }	
						echo '<b>File:</b> '.$friendly_name.'<br />';
						echo '<form action="'.$_SERVER['PHP_SELF'].'" method="post">';
						echo '<textarea name="contents" id="contents" cols="40" rows="10">'.stripslashes($contents).'</textarea><br />';
						echo '<input type="hidden" name="p" value="'.$p.'" />';
						echo '<input type="hidden" name="a" value="update" />';
						echo '<input type="hidden" name="file" value="'.$file.'" />';
						echo '<input type="submit" name="sub" value="Update" />&nbsp;';
						echo '<input type="button" onclick="popup(\'preview.php\',\'400\',\'300\',\'yes\',\'preview\')" value="Preview" />';
						echo '</form>';
					}
					else
					{
						echo "Could not read contents of file '".$friendly_name."'.";
					}
				}
				else
				{
					echo "Could not fopen file '".$friendly_name."'.";
				}
				
				
			}
			else
			{
				echo "File '".$friendly_name."' does not exist.";
			}
		}
		else
		{
			echo 'No filename provided.';
		}
		echo '</div>';
		echo '<div class="mainHolderFooter">&nbsp;</div>';
	}
}
else
{
	echo '<div class="mainHolderHeader">Welcome</div>';
	echo '<div class="mainHolder">';
	echo 'Welcome to the Admin Area. Please click on a file in the left menu to edit its content.';
	echo '</div>';
	echo '<div class="mainHolderFooter">&nbsp;</div>';
}
	
?>