<HTML>
<HEAD>
<TITLE>JMM</TITLE>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="css/cadogan_styles.css" type="text/css">
<script language="JavaScript" type="text/JavaScript">
<!--

<!--
function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);
//-->
</script>
</HEAD>
<BODY BGCOLOR=ffffff leftmargin="20" topmargin="20" marginwidth="20" marginheight="20">
<!-- DO NOT MOVE! The following AllWebMenus code must always be placed right AFTER the BODY tag-->
<!-- ******** BEGIN ALLWEBMENUS CODE FOR JMM ******** -->
<span id='xawmMenuPathImg-JMM' style='position:absolute;top:-50px'><img name='awmMenuPathImg-JMM' id='awmMenuPathImg-JMM' src='./awmmenupath.gif' alt=''></span><script type='text/javascript'>var MenuLinkedBy='AllWebMenus [2]', awmBN='528'; awmAltUrl='';</script>
<script type='text/javascript'>awmBuildMenu();</script>
<!-- ******** END ALLWEBMENUS CODE FOR JMM ******** -->
<table width="800" border="0" cellpadding="2" bgcolor="#FFFFFF">
  <tr> 
    <td colspan="3"><img src="image/jmmbiogheader.jpg" width="800" height="250"></td>
  </tr>
  <tr> 
    <td width="75" height="193">&nbsp;</td>
    <td width="713" align="left" valign="top" bgcolor="#FFFFFF">
<p><span style="font-family:times;font-size:16px;"><span style="font-family:arial;font-size:13px;color:white;"><font color="#29298C" size="4" face="Arial Black">Paul 
        Gascoigne </font></span></span></p>
      <p><span style="font-family:times;font-size:16px;"><span style="font-family:arial;font-size:13px;color:white;"><font color="#000000">One 
        of the most gifted footballers of </font></span><span style="font-family:arial;font-size:13px;"><font color="#000000">all time </font></span><span style="font-family:arial;font-size:13px;color:white;"><font color="#000000">, Paul has 
        had more column inches devoted to him than virtually any other footballer. 
        </font></span></span></p>
      <p><span style="font-family:times;font-size:16px;"><span style="font-family:arial;font-size:13px;color:white;"><font color="#000000">In 
        a career which has spanned over 20 years, Paul signed for his hometown 
        club, Newcastle United, aged 17 and played with distinction for the Magpies 
        for four seasons. In July 1988 he was lured south by Tottenham Hotspur 
        in a deal worth &pound;2million, making his debut, ironically, against 
        Newcastle United. A few months later, Paul made his England debut as a 
        substitute in a friendly against Denmark. In April 1989 he scored his 
        first international goal, in another substitute appearance against Albania 
        at Wembley.</font></span></span></p>
      <p><span style="font-family:times;font-size:16px;"><span style="font-family:arial;font-size:13px;color:white;"><font color="#000000">An 
        outstanding 1989/90 season at Spurs coupled with an impressive start to 
        his international career, prompted the England manager, Bobby Robson, 
        to include Paul in his final squad of 22 for the 1990 World Cup Finals 
        in Italy. &quot;Gazza&quot;, as he was affectionately known, was outstanding, 
        operating in the heart of England&#8217;s midfield and, for the first 
        time, earning himself a reputation outside Britain as a truly gifted player. 
        England&#8217;s run to the semi finals captivated the country and Paul 
        earned himself a place in the hearts of the nation with the tears he shed 
        at picking up a booking which would have ruled him out of the Final had 
        England reached it. </font></span></span></p>
      <p><span style="font-family:times;font-size:16px;"><span style="font-family:arial;font-size:13px;color:white;"><font color="#000000">Paul 
        returned from Italia '90 a world superstar, both on and off the pitch. 
        However, in the F A Cup Final of 1991 a rash tackle led to him being stretchered 
        off with torn cruciate ligaments in his right knee. After a long period 
        of recovery, Paul was finally passed fit in May 1992 and was able to complete 
        his long since negotiated &pound;5.5million move to Italian club Lazio, 
        making his debut in September 1992. Although he turned in some outstanding 
        displays, overall Paul&#8217;s time in Italy was unhappy and plagued by 
        injury and he left in June 1995 to join Scottish giants Rangers.</font></span></span></p>
      <p><span style="font-family:times;font-size:16px;"><span style="font-family:arial;font-size:13px;color:white;"><font color="#000000">After 
        a slow start, Paul began to find his feet towards the end of the 1995/96 
        season, culminating in a wonderful hat-trick on the final day of the season 
        against Aberdeen to win Rangers their eighth Championship in a row. Named 
        Scottish Footballer of the Year, Paul was selected for the England squad 
        for the 1996 European Championships. In what was arguably the most memorable 
        moment of a glittering career, he scored a fantastic goal against Scotland 
        in the group stage to secure a crucial win for his country. Fully fit 
        for the first time in years, Paul played a pivotal role in England&#8217;s 
        progress to the semi final stage, where once again they lost on penalties 
        to Germany.</font></span></span></p>
      <p><span style="font-family:times;font-size:16px;"><span style="font-family:arial;font-size:13px;color:white;"><font color="#000000">Two 
        more years at Rangers followed, winning the 1997 League Championship and 
        League Cup, before Paul moved to Middlesbrough midway through the 1997/98 
        season. His controversial exclusion from the 1998 World Cup squad marked 
        the end of his international career. Since then, Paul has also played 
        club football for Everton, Burnley and Chinese team Gansu Tianma and had a brief spell as manager of Kettering Town FC.</font></span></span></p>
      <p><span style="font-family:times;font-size:16px;"><span style="font-family:arial;font-size:13px;color:white;"><font color="#000000">Paul 
        holds a unique place in the affection of the British public. His autobiography, <em>Gazza - My Story, </em>
        was published in the summer of 2004 and topped the best sellers list, 
        winning Sports Book of the Year at the British Book Awards. In 2006 he 
        wrote <em>Being Gazza</em> with his therapist John McKeown, which also 
        topped the best sellers list</font></span><span style="font-family:arial;font-size:13px;"><font color="#000000"> and took part in <em>Soccer Aid </em>(ITV) and <em>The Match (</em>Sky One).&nbsp; Paul  also contributed to Bravo's coverage of <em>Football Italia </em>during the 2006/07 season. </font></span></span></p>
      <p>&nbsp;</p>
      <p><span style="font-family:arial;"><em><font color="#000000" size="1">June</font></em></span><span style="font-family:times;font-size:16px;"><span style="font-family:arial;font-size:13px;color:white;"><font color="#000000"><em><font size="1"> 200</font></em></font></span></span><span style="font-family:times;"><span style="font-family:arial;"><font color="#000000"><em><font size="1">7</font></em></font></span></span></p></td>
    <td width="75" align="left" valign="top">&nbsp;</td>
  </tr>
</table>
</BODY>
</HTML>
