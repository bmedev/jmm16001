





PETER ALLISS


Born in 1931 in Berlin, the name of Peter Alliss would appear on many people's list of the top 50 golfers since the war.

The son of Percy Alliss, one of the foremost British professionals between the wars, Peter lost no time in following in his father's footsteps.  He turned professional in 1946 at the age of 15 and played in his first Open a year later.  Although he never succeeded in winning any of the major championships, Peter's victory tally in European tournaments was impressive.  He won 20 titles between 1954 and 1969 including the Italian, Spanish and Portuguese Opens in the space of three weeks in 1958.  Peter's first appearance in the Ryder Cup was in 1953 and, with the single exception of 1955, he represented Great Britain and Ireland in the event until 1969.

Peter's retirement from international golf at the early age of 38 led to a successful new career as a broadcaster, writer and golf course design consultant.  He joined the BBC commentary team in 1961 and has become known to millions of viewers in America, Canada and Australia as well as the UK.  He enjoyed a legendary commentating partnership with the late Henry Longhurst and also hosted the popular Pro-Celebrity Golf programmes and Around With Alliss.  His cheerful and informative presentation has helped to convert many non-golfers to the sport.

An accomplished writer, Peter's credits include the series of Bedside Golf books, Peter Alliss - An Autobiography, The Who's Who of Golf, Peter Allis's Golf Heroes and a novel published in 1983 - The Duke.  His definitive autobiography, My Life, was published in October 2004.

Among the golf courses Peter has designed and constructed in partnership with David Thomas is The Belfry at Sutton Coldfield, national headquarters of the PGA and a Ryder Cup venue.

November 2004
