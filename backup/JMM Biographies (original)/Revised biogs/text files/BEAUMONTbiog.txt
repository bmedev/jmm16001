






BILL BEAUMONT OBE


Born in Preston in 1952, Bill first pulled on a Fylde jersey in 1970 when he was full back for the sixth team - two years later he was a forward in the first team.

It is as a great leader and captain that he will always be remembered.  His triumphs in 1980 will never be forgotten - he led England to their first Grand Slam since 1957 and was captain of the British Lions.

Bill's international career started in 1975 against Ireland and he played for England 34 times.  His first involvement with the Lions was in 1977 when he was flown out to New Zealand as a replacement.  He played three games but the greatest honour of his career was when he was invited to captain the British Lions in South Africa in 1980.  The association continues as Bill is to be the manager of the 2005 touring party to New Zealand.

Sadly a head injury, while playing for Lancashire in the 1982 County Championship final, prematurely ended his career.  However he is just as well known these days for his television and media work - Bill was the longest serving team captain on BBC Television's A Question of Sport with over 300 appearances in 13 years and has been a studio analyst and expert summariser for the BBC and Sky Sports.  He is also chairman of the Rugby Football Union playing committee.

Bill is married with three sons and is managing director of the family mill which was founded by his grandfather in Chorley, Lancashire.  He was awarded the OBE in 1982 for services to rugby.

November 2004

