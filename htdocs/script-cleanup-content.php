<? 
function cleanup_files($content_path, $extensions, $print)
{
	
	$output = "";
	$count = 1;
	if(substr($content_path, strlen(trim($content_path))-1, 1)!="/") { $content_path .= '/'; }
	if(file_exists($content_path))
	{
		$dir=opendir($content_path); 	
		while($file=readdir($dir))
		{
		   if ($file!="." and $file!="..") 
		   {

			  if (is_dir($content_path.$file)) 
			  {
				$output .= cleanup_files($content_path.$file, $extensions, "n");
			  }
			  else
			  {
			  //	echo $count." (file)<br />";
					if(strlen(trim($extensions))>0)
					{	
						$myExp = explode(".",$file);
						$myExt = trim(strtolower($myExp[count($myExp)-1]));
						unset($valid_exts);
						$validExp = explode("|", trim(strtolower($extensions)));
						foreach($validExp as $val)
						{
							$valid_exts[$val] = "y";
						}
					}
					if((strlen(trim($extensions))==0)||($valid_exts[$myExt] == "y"))
					{
						$tmpString = "";
						$lines = file($content_path.$file);
						foreach ($lines as $num => $val) 
						{
							$tmpString .= trim($val)." ";
						}
						$tmpString = eregi_replace("\n|\r","",trim($tmpString));
						$tmpString = eregi_replace("</p>","^*177*^",trim($tmpString));
						$tmpString = strip_tags(trim($tmpString),"<i><em>");
						$tmpString = eregi_replace("\^\*177\*\^","<br /><br />",trim($tmpString));
						//$tmpString = eregi_replace("<p>","",trim($tmpString));
						//$tmpString = eregi_replace("<p style=\".+\">","",trim($tmpString));
						//$tmpString = eregi_replace("<p style='.+'>","",trim($tmpString));
						//$tmpString = eregi_replace("</p>","<br /><br />",trim($tmpString));
						
						
						
						if($fp = fopen($content_path.$file, "w"))
						{
							if(fwrite($fp, $tmpString))
							{
								$output .= "successfully updated file '".$file."'.<br />";
							}
							else
							{
								$output .= "Could not write file '".$file."'.<br />";
							}
							fclose($fp);
						}
						else
						{
							$output .= "Could not fopen file '".$file."'.<br />";
						}
								
				
					}
			  }
			  $count++; 
		   }
		}
		$output .= "<br />";
		closedir($dir); 
	}
	else
	{
		$output .= "Content Path does not exist.<br />";
	}
	
	if($print=="y") { echo $output; }
	else {	return $output; }
}

cleanup_files("/www/vhtdocs/jmm-ftp/content/", "txt", "y");
?>