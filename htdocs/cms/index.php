<? 
	include("init.php");
?>
<html>
<head>
<title>Admin Area</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="style.css" type="text/css" />
<script type="text/javascript">
	function popup(page,width,height,scrollbars,popname)
	{
		var screenW = screen.availWidth;
		var screenH = screen.availHeight;
		var leftP;
		var topP;
		leftP = (screenW-width)/2;
		topP = (screenH-height)/2;
		var win = window.open(page,popname,"width="+width+",height="+height+",location=no,toolbar=no,scrollbars="+scrollbars+",resizable=no,menubar=no,left="+leftP+",top="+topP);
		win.focus();
	}
</script>
</head>

<body>
<div class="topBanner"><table cellpadding="0" cellspacing="0">
<tr>
	<td width="10">&nbsp;</td>
	<td height="37">Admin Area</td>
	<td width="40">&nbsp;</td>
	<td><a href="adminsystem_intro.pdf" target="_blank">Help</a></td>
</tr>
</table></div>
<table cellpadding="0" cellspacing="0" width="100%">
	<tr>
		<td width="10" valign="top"><img src="images/spacer.gif" width="10" height="1" /></td>
		<td width="210" valign="top"><? include("tab_left_menu.php"); ?></td>
		<td width="10" valign="top"><img src="images/spacer.gif" width="10" height="1" /></td>
		<td valign="top"><?  include("tab_main_screen.php");  ?></td>
		<td width="10" valign="top"><img src="images/spacer.gif" width="10" height="1" /></td>
	</tr>
</table>
</body>
</html>
