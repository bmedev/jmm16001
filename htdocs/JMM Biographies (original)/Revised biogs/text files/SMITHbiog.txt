






MATT SMITH


Matt is one of ITV's main sports broadcasters.  He presents their Sunday Football League programme The Championship and also hosts their UEFA Cup coverage and some of the UEFA Champions League output. 

Matt started his career at the BBC working on Football Fever, World Snooker and covering international events including the Sydney Olympics and Euro 2000, before moving to ITV in the summer of 2001.  During the 2002 World Cup Matt hosted a panel based show, featuring debate and match highlights every evening during the tournament.  The success of this programme led to Matt establishing himself as an irreplaceable part of the ITV sports team as the format was adopted for The Premiership On Monday which he also presented.  Matt has also hosted On The Ball and Speed Sunday and at Euro 2004 was the senior ITV reporter with the England team.

Born in Liverpool in 1967, Matt now lives in London with his wife and two children.

November 2004
