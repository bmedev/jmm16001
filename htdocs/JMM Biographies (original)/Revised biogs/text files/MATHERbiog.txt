


VICTORIA MATHER


A noted writer and broadcaster, Victoria is one of the foremost social commentators of our time.  Her feature writing for Harpers & Queen, GQ and Tatler has covered many subjects from The Sloane Ranger Handbook to the coal miners' strike.  It is, however, as a travel writer and editor of the Tatler Travel Guide and the infamous 101 Best Hotels in the World and her creation of the Tatler Travel Awards that she is best known.  She has recently left Tatler to take up the position of travel editor of Vanity Fair and she has a column in the Daily Telegraph's new quarterly travel magazine called Victoria's Secrets where she gives tips to the discerning traveller.

Since 1986, Victoria has been a film critic and columnist for The Daily Telegraph.  Her brilliant Social Stereotypes column, wonderfully illustrated by Sue Macartney-Snape, has proved such a hit with readers that six anthologies have been produced, three of which have been best sellers.

During Victoria's time at the Evening Standard, she covered many events including the Academy Awards for seven years, the Dubai World Cup, the Krug Party and the Chelsea Flower Show.  She also wrote a regular London social comment column.

Victoria's broadcasting career has varied from reporting on the funerals of Diana, Princess of Wales and HM Queen Elizabeth The Queen Mother for ABC/NBC/CBS News to her most recent work, The Dinner Party Inspectors.  She co-presented this very popular series with Meredith Etherington-Smith for C4 in 2003.  Since 1986 she has also been a regular contributor to BBC Radio 4's Loose Ends, hosted by Ned Sherrin and in October 2004 she made her debut in Dictionary Corner on Channel 4's Countdown.

Victoria grew up in Yorkshire, North Wales and Greece.  She now spends her time in either London, Hampshire or on aeroplanes!  She has one Pekinese dog, Bubble, one husband and eleven Godchildren to keep her busy.

November 2004
