




SIR TREVOR BROOKING CBE


Born in 1948 in Barking, Trevor joined West Ham United as an apprentice in 1965.  He spent his whole playing career with the club until retiring in 1984, making over 600 appearances and scoring over 100 goals.  He was an FA Cup winner in 1975 and 1980, scoring the only goal of the latter match against Arsenal with a header.  A former non-executive director of the club, Trevor was acting manager/coach for a short period during the 2002/3 season.

Trevor made his debut for England in 1974 against Portugal, was capped 47 times and scored five goals.  He played under four managers:  Sir Alf Ramsey, Joe Mercer, Don Revie and Ron Greenwood.  He retired from international football after the 1982 World Cup.

Trevor was awarded the MBE in 1981.  At about the same time, another honour in recognition of his services to football was bestowed upon him when the West Ham local council named a road after him - Brooking Road.  In 1989 he was appointed to the Sports Council as the representative of regional chairmen and then Chairman of the renamed Sport England.  In recognition of this work for the Sports Council, Trevor was awarded the CBE in the 1999 New Year's Honours, followed by a knighthood in the Queen's Birthday Honours in June 2004.

In 1984 Trevor joined BBC Radio and Television and for 20 years was one of the most respected analysts and commentators in football.  A regular on Match of the Day and Radio Five Live, he was part of the BBC's successful coverage of the World Cup in 1998 and 2002 and the European Championships in 1996 and 2000.  In 2004 Trevor was appointed Director of Football Development at The Football Association.

November 2004
