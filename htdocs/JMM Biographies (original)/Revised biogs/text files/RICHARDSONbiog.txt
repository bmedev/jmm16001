




JAMES RICHARDSON


James has always been associated with Italian football and can be seen on British Eurosport where he anchors their Sunday Italian football show Sunday Scudetto.  He also presents all the live Brazil matches on Bravo as well as voicing their weekly Premiership Diaries - a fly on the wall programme following the lives of Robbie Savage and Edu.

He began his career in broadcasting behind the camera as a producer for Eurosport and Sky Sports before making the move into the spotlight.  His relaxed style, sharp wit and love and knowledge of all things Italian have attracted a cult following.  For many years the face and voice of Italian football, James returned permanently to the UK in 2003 after ten years in Rome. Whilst in Italy he presented, wrote and produced Channel 4's Serie A output, the magazine show Gazzetta Football Italia and Football Italia live games.  A gifted linguist, James also speaks French, Spanish and Portuguese.

James co-hosted general knowledge superquiz Grand Slam with Carol Vorderman on C4 and presented British Eurosport's coverage of Euro 2004.  He is also a columnist for Media Guardian and has provided voice overs for computer games and radio and television commercials.

James is married with one son.

November 2004   
