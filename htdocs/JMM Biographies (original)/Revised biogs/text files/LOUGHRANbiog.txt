







ANGUS LOUGHRAN


Angus began his career in sports broadcasting in 1986 by commentating into Ladbroke's betting shops for five years, developing their in-house service until the arrival of SIS who took it to a different level by bringing live pictures into the shops.  In 1991, he went freelance and covered his first Olympic Games for Eurosport, the Winter Olympics in Albertville.

Angus is renowned for his insight into the world of gambling and is a regular member of BBC Television's racing team, presenting the latest betting news from all the major meetings including Ascot, Aintree, Epsom, Goodwood and Haydock.  He writes a very successful betting column in The Daily Telegraph's Friday sports section.  His intense knowledge of European football is essential for the commentaries which have taken him all over the world.  He covers the Champions League for ESPN International, makes regular reports into BBC Radio Five Live and has commentated on Chinese football for Channel 4, Spanish football for South African television and English Premiership matches for overseas channels.  He has also covered lesser known events including husky racing, indoor rock climbing and ballroom dancing.

During the first BBC2 series and when it transferred to ITV for the 1998 World Cup and Euro 2004, Angus was part of the Fantasy Football team portraying "Statto", the character created by Frank Skinner and David Baddiel.  He has also appeared on Central Television's sports magazine programme, Late Tackle.

November 2004
